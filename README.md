**O Projeto :**

Nosso projeto consiste em coletar as cotações, Data,Abertura,Media,Maxima,Minima e Variacao do Dolar para que se possa relacionar com demais informacoes e salvando em um arquivo CSV,
 minerando e explorando um site https://br.investing.com/. O projeto foi trabalhado também e postado no GitHub, uma plataforma semelhante ao GitLab executando a mesma funções semelhantes.
 
Link do GitHub:

https://github.com/pan963/Crawler

**Documentação:**

[Cronograma_Indicadores_Econômicos.odp](/uploads/093c9decfbd177420da32f156b576bb7/Cronograma_Indicadores_Econômicos.odp)

[Curriculum_Vitae_Joao_L.docx](/uploads/68701f7ad975bef9afe885a82a7b8ab7/Curriculum_Vitae_Joao_L.docx)

[Matriz_de_habilidade.pdf](/uploads/de0f046e4db17d94cc837cd6d2ef00d0/Matriz_de_habilidade.pdf)

[MvpProgetoIntegrador.pdf](/uploads/e4f0f2134895760b561259df4177b745/MvpProgetoIntegrador.pdf)

[Roteiro_Bot.pdf](/uploads/fa640ce4db199275ad157952c9b28c9b/Roteiro_Bot.pdf)

[Sprint_1.pdf](/uploads/b9e593bfa98bbf65e5e8a88cc7e951a9/Sprint_1.pdf)

**Equipe:**

Carlos Henrique Soares

Joao Luiz Pan Toratti

Robson da Silva Campello Junior


**Como utilizar o Crawler**

Baixe o Script Bot uma moeda ou BotAutomatizado e execute no diretorio ou Vscode ou Idle.(para o BotdeUmaMoeda e necessario colocar os parametro da moeda):

[BotAutomatizadopara_todas_as_moedas.py](/uploads/01fecaaec262abb24dce4c259d8ae961/BotAutomatizadopara_todas_as_moedas.py)

[BotManualUmamoeda.py](/uploads/9b1a0a214b0b98ad0cb2c13cdc0f04c2/BotManualUmamoeda.py)


**Python**

O Crawler foi estruturado em Crawler, portanto e necessario ele para executa-lo.

sudo apt-get install python3-pip

**Bibliotecas**

Foram usadas as seguintes Bibliotecas:

requests

csv

time

multiprocessing

logging

datetime

lxml html

**bs4:**

pip3 install bs4

**requests:**

pip3 install requests

**lxml:**

pip3 install lxml

**pymongo:**

pip3 install pymongo

pip3 install dnspython

**Para verificar se as bibliotecas foram instaladas basta utilizar o comando:**

pip3 list

